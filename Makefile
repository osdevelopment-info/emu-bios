
all: prepare source tex bin pdf

prepare:
	mkdir -p build

source:
	$(MAKE) -C src source

bin:
	$(MAKE) prepare
	$(MAKE) -C src bin
	cp src/bios86 build/
	cp src/bios286 build/
	cp src/bios386 build/

.PHONY: tex
tex:
	$(MAKE) -C tex tex

pdf:
	$(MAKE) prepare
	cp *.tex tex/
	$(MAKE) -C tex pdf
	mv tex/biosx86.pdf build/

clean: mostlyclean
	$(MAKE) -C src clean
	$(MAKE) -C tex clean

mostlyclean:
	$(MAKE) -C src mostlyclean
	$(MAKE) -C tex mostlyclean
	rm -f *~
	rm -rf build/
