#!/bin/sh
cd $(dirname $0)
docker run -it -d -e "TERM=xterm-256color" -v `pwd`:`pwd` \
  -v $HOME/.bashrc:/root/.bashrc -w `pwd` \
  --name emu-bios-noweb -h emu-bios-noweb \
  registry.sw4j.net/sw4j-net/noweb/master:latest
docker run -it -d -e "TERM=xterm-256color" -v `pwd`:`pwd` \
  -v $HOME/.bashrc:/root/.bashrc -w `pwd` \
  --name emu-bios-nasm -h emu-bios-nasm \
  registry.sw4j.net/sw4j-net/nasm/master:latest
docker run -it -d -e "TERM=xterm-256color" -v `pwd`:`pwd` \
  -v $HOME/.bashrc:/root/.bashrc -w `pwd` \
  --name emu-bios-jekyll -h emu-bios-jekyll \
  registry.sw4j.net/sw4j-net/jekyll/master:latest
